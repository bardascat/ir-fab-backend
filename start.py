from flask import Flask, request
from flask_cors import cross_origin
import logging
from IRService import IRService
app = Flask(__name__)

irService = IRService()



logging.basicConfig(filename="output.txt",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

logging.info("Started Script")
logger = logging.getLogger("start.py")
@app.route("/")
def index():
    return "<h1>IR Service</h1>"


@app.route('/event', methods=['POST'])
@cross_origin()
def event_post():
  body = request.get_json()

  if "remote" not in body or "action" not in body:
    logger.info("Invalid payload" + str(body))
    return '', 400

  logger.info("remote: " + body["remote"])
  logger.info("action: " + body["action"])

  irService.performEvent(body['remote'], body['action'])
  return '', 204

# @app.route('/streamer', methods=['POST'])
# @cross_origin()
# def streamer_post():
#   body = request.get_json()
#
#   logger.info("Streamer post")
#   if "action" not in body or "value" not in body:
#     logger.info("Invalid payload" + str(body))
#     return '', 400
#
#   logger.info("action: " + body["action"])
#   logger.info("value: " + str(body["value"]))
#
#   if body['action'] == "PLAY_PLAYLIST":
#     spotifyService.playPlaylist(body['value'])
#   elif body['action'] == "PLAY_ARTIST":
#     spotifyService.playArtist(body['value'])
#   elif body['action'] == "PLAY_NEXT":
#     spotifyService.playNextTrack()
#   elif body['action'] == "PLAY_PREVIOUS":
#     spotifyService.playPrevTrack()
#   elif body['action'] == "PAUSE_MUSIC":
#     spotifyService.pauseMusic()
#   elif body['action'] == "PLAY_MUSIC":
#     spotifyService.startMusic()
#   else:
#     logger.info("Invalid action:" + body['action'])
#
#   return '', 204
#

app.run(host='0.0.0.0', port= 8090)