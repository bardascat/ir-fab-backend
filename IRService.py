import os
import logging

from remote import RemoteEnum
class IRService:
  def __init__(self):
    print("IRService initialized")

  def performEvent(self, remote, action):
    self.logger = logging.getLogger("IrService performEvent")

    if remote.upper() == RemoteEnum.NAD.name:
      self.__performNadEvent(action)
    else:
      self.logger.info("Invalid remote name: " + remote)


  def __performNadEvent(self, action):
    self.logger.info("__performNadEvent" + action)
    self.__perform(self.__generateAction('monoceros', action))

  def __generateAction(self, remote, action):
    command = "irsend SEND_ONCE "+ remote.lower() + " " + action
    self.logger.info("executing command:" + command)
    return command

  def __perform(self, action):
    os.system(action)
